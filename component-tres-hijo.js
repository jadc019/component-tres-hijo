{
  const {
    html,
  } = Polymer;
  /**
    `<component-tres-hijo>` Description.

    Example:

    ```html
    <component-tres-hijo></component-tres-hijo>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --component-tres-hijo | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class ComponentTresHijo extends Polymer.Element {

    static get is() {
      return 'component-tres-hijo';
    }

    static get properties() {
      return {
        url: {
          type: String, value:"https://pokeapi.co/api/v2/pokemon/1/"
        },
        pokemon: Object
      };
    }

    ready() {
      super.ready();
      console.log('Ejecutándose ready');
      this.$.pokemonDPDetail.generateRequest();
    }

    _result(evt) {
      console.log('_result', evt);
      this.pokemon = evt.detail;
    }

    static get template() {
      return html `
      <style include="component-tres-hijo-styles component-tres-hijo-shared-styles"></style>
      <slot></slot>
      
      <cells-generic-dp
        id="pokemonDPDetail"
        host="[[url]]"
        path=""
        method="GET"
        on-request-success='_result'>
      </cells-generic-dp>

      <img src="[[pokemon.sprites.front_default]]" width="250" height="250"></img>
      `;
    }
  }

  customElements.define(ComponentTresHijo.is, ComponentTresHijo);
}